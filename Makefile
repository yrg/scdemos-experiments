.PHONY: all clean

all:
	dune build src/driver.exe src/fragment.exe
	ln -sf _build/default/src/fragment.exe
	ln -sf _build/default/src/driver.exe

clean:
	dune clean
	rm -fr fragment.exe driver.exe
