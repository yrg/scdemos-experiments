type color = { r : int; g : int; b : int }

let black = { r = 0; g = 0; b = 0 }

let white = { r = 255; g = 255; b = 255 }

type t = { w : int; h : int; pixels : color array array }

let make w h f =
  let pixels = Array.make_matrix w h white in
  for x = 0 to w - 1 do
    for y = 0 to h - 1 do
      pixels.(x).(y) <- f x y
    done
  done;
  { w; h; pixels }

let write filename img =
  let temp = Filename.temp_file "fragment" ".ppm" in
  let cout = open_out temp in
  let fmt = Format.formatter_of_out_channel cout in
  let open Format in
  fprintf fmt "P3\n";
  fprintf fmt "%d %d\n" img.w img.h;
  fprintf fmt "255\n";
  for y = 0 to img.h - 1 do
    for x = 0 to img.w - 1 do
      let c = img.pixels.(x).(y) in
      fprintf fmt "%d %d %d\n" c.r c.g c.b
    done
  done;
  flush cout;
  close_out cout;
  Unix.rename temp filename
  (*  Unix.unlink temp *)
