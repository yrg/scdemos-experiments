(* ffmpeg -framerate 10 -i image%d.ppm -c:v libx264 -crf 25 -vf "scale=500:500,format=yuv420p" -movflags +faststart output.mp4 *)
let () =
  if Array.length Sys.argv < 7 then
    failwith "driver.exe outdir width height framerate split duration"

let outdir = Sys.argv.(1)

let width = int_of_string Sys.argv.(2)

let height = int_of_string Sys.argv.(3)

let framerate = int_of_string Sys.argv.(4)

let split = int_of_string Sys.argv.(5)

let nb_workers = split * split

let duration = float_of_string Sys.argv.(6)

let generator_process i =
  let path = Filename.concat outdir (string_of_int i) in
  ignore @@ Sys.command (Format.asprintf "mkdir -p %s" path);
  let cmd, args = "./fragment.exe", [
    "fragment.exe";
    path;
    string_of_int i;
    string_of_int @@ width / split;
    string_of_int @@ height / split;
    string_of_int @@ framerate
  ]
  in
  let pid =   Unix.process_pid @@ Unix.open_process_args cmd (Array.of_list args) in
  Format.eprintf "generator | pid = %d | cmd = %s %s\n%!" pid cmd (String.concat " " args);
  pid

let uri i = Format.asprintf "udp://127.0.0.1:%d" (i + 1024)

let video_process i =
  let path = Filename.(concat (concat outdir (string_of_int i)) "img%6d.ppm") in
  let cmd, args =
    "/usr/bin/ffmpeg", [
      "ffmpeg";
      "-re";
      "-loglevel"; "-8";
      "-stream_loop"; "-1";
      "-framerate"; string_of_int framerate;
      "-r"; string_of_int framerate;
      "-i"; path;
      "-preset"; "ultrafast";
      "-vcodec"; "libx264";
      "-tune"; "zerolatency";
      "-b:v"; "100k";
      "-r"; "10";
      "-f"; "mpegts";
      uri i
    ]
  in
  Format.eprintf "video cmd = %s %s\n%!" cmd (String.concat " " args);
  let c =
    Unix.open_process_args cmd (Array.of_list args)
  in
  Unix.process_pid c
(*  ffmpeg -stream_loop -1 -framerate 10 -i out/img%4d.ppm -preset ultrafast -vcodec libx264 -tune zerolatency -b:v 100k -f mpegts udp://127.0.0.1:1234 *)

let pids = ref []

let push pid = pids := pid :: !pids

let () = at_exit @@ fun () ->
  List.iter (fun pid ->
      Format.eprintf "Killing %d\n%!" pid;
      Unix.kill pid 15) !pids

(* let worker i =
 *   push @@ generator_process i;
 *   Unix.sleepf 1.;
 *   push @@ video_process i *)

(*
-filter_complex " \
      [0:v] setpts=PTS-STARTPTS, scale=qvga [a0]; \
      [1:v] setpts=PTS-STARTPTS, scale=qvga [a1]; \
      [2:v] setpts=PTS-STARTPTS, scale=qvga [a2]; \
      [3:v] setpts=PTS-STARTPTS, scale=qvga [a3]; \
      [a0][a1][a2][a3]xstack=inputs=4:layout=0_0|0_h0|w0_0|w0_h0[out] \
      " \
    -map "[out]" \
    -c:v libx264 *)

let rec range start stop =
  if start = stop then [start] else start :: range (start + 1) stop

let _mosaic () =
  let inputs =
    List.(flatten @@
          map (fun i -> [ "-i"; uri i ]) (range 1 (nb_workers)))
  in
  let xstack =
    " [0:v] setpts=PTS-STARTPTS, scale=qvga [a0]; \
      [1:v] setpts=PTS-STARTPTS, scale=qvga [a1]; \
      [2:v] setpts=PTS-STARTPTS, scale=qvga [a2]; \
      [3:v] setpts=PTS-STARTPTS, scale=qvga [a3]; \
      [a0][a1][a2][a3]xstack=inputs=4:layout=0_0|0_h0|w0_0|w0_h0[out] "
  in
  (*  let xstack = "xstack=inputs=4:grid=2x2" in *)
  let filter = [
    "-filter_complex";
    Format.asprintf "\"%s\"" xstack;
    "-map"; "[out]"
  ] in
  let args =
    [ "";
      "-loglevel"; "+debug";
      "-framerate"; string_of_int framerate
    ] @ inputs
    @ filter
    @ [
      (* "-preset"; "ultrafast";
       * "-vcodec"; "libx264";
       * "-tune"; "zerolatency";
       * "-b:v"; "100k"; *)
      (* "-f"; "mpegts";
            uri 0 *)
      "-c:v"; "libx264"; "-t"; "10";
      "-f"; "matroska"; "output_col_2x2.mkv"
    ]
  in
  let c =
  Unix.open_process_args "/usr/bin/ffmpeg" (Array.of_list args)
  in
  Unix.process_pid c

let () =
  for i = 1 to nb_workers do
    push @@ generator_process i;
  done;
  Unix.sleepf 10.;
  for i = 1 to nb_workers do
    push @@ video_process i
  done;
  (*  push @@ mosaic ();*)
  Unix.sleepf (duration -. 10.);
  exit 0
