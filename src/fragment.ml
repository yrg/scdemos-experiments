let () =
  let nb_args = Array.length Sys.argv in
  if nb_args <> 6 then (
    Printf.eprintf "Got %d / %s\n" nb_args (String.concat " " (Array.to_list Sys.argv));
    failwith "fragment.exe outdir seed width height framerate"
  )

let outdir = Sys.argv.(1)

let seed = int_of_string Sys.argv.(2)

let width = int_of_string Sys.argv.(3)

let height = int_of_string Sys.argv.(4)

let framerate = float_of_string Sys.argv.(5)

let filename i =
  Format.asprintf "%s/img%06d.ppm" outdir i

let abs x = if x < 0 then -x else x

let color i x y =
  Image.{ r = (abs (i * x)) mod 256; g = (abs (i * y)) mod 256; b = (abs (i * (x + y))) mod 256 }

let image i =
  Image.make width height (color i)

let window = 10

let rec generate seed i =
  (* Printf.eprintf "%d%!\033[2K\r" i; *)
  let tstart = Unix.gettimeofday () in
  Image.write (filename (i mod window)) (image (seed + i));
  let tstop = Unix.gettimeofday () in
  Unix.sleepf (max 0. (1. /. framerate -. (tstop -. tstart)));
  generate seed (i + 1)

let () =
  ignore @@ Sys.command (Format.asprintf "rm -fr %s; mkdir -p %s" outdir outdir);
  for i = 0 to window - 1 do
    ignore @@ Sys.command ("touch " ^ filename i);
  done;
  generate seed 0
